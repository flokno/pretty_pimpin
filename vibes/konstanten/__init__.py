# flake8: noqa
from ase import units

from .einheiten import *

v_unit = units.Ang / (1000.0 * units.fs)
