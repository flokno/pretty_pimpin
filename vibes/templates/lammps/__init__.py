"""
Provide readily prepared lammps calculators
"""
# flake8: noqa

from .setup import setup_lammps_gan, setup_lammps_si
