""" Use the vibes.phonopy workflow to i) run the necessary force calculations
    and ii) create a phonopy object including force constants etc. """

from vibes import run_phono3py

run_phono3py()
